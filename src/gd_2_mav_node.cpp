#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <math.h>

#define PI 3.14159265

bool odom_rec = false;

// Global Vars
// Visual Pose
geometry_msgs::PoseStamped vis_pos;
// Ultra Msg
sensor_msgs::LaserScan ultra_msg;
// Guidance Odom
nav_msgs::Odometry godom_msg;

// Guidance Ultrasonics Callback
void guidanceUltraCallback(const sensor_msgs::LaserScan& msg)
{
	ultra_msg = msg;
}

// Guidance Odom Callback
void guidanceOdomCallback(const nav_msgs::Odometry& msg)
{
	godom_msg = msg;
	odom_rec = true;
}

int main(int argc, char **argv)
{ 
  //======= ROS Setup ================
  ros::init(argc, argv, "gd_2_mav_node");
  ros::NodeHandle n;
  //======= Vars ================
  //======= ROS Publishers ================
  ros::Publisher pose_publisher = n.advertise<geometry_msgs::PoseStamped>("pose", 1000);
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);    
  // ROS setup subscriber
  ros::Subscriber guid_ult_sub = n.subscribe("guid_ult",1,guidanceUltraCallback);
  ros::Subscriber guid_odom_sub = n.subscribe("guid_odom",1,guidanceOdomCallback);
  // Start the Spinner
  spinner.start();
  //ros::spin();
  
  while (ros::ok())
  {
    if(odom_rec){
        vis_pos.header = godom_msg.header;
    	vis_pos.pose.position.x = godom_msg.pose.pose.position.z;
	    vis_pos.pose.position.y = godom_msg.pose.pose.position.x;
	    vis_pos.pose.position.z = ultra_msg.ranges[0]-0.05;
	    odom_rec = false;
	    pose_publisher.publish(vis_pos);
	}  
  }
  return 0;
}
